/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saharat.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author saharudseerakoon
 */
public class TestReceipt {

    public static void main(String[] args) {
        Product p1 = new Product(1, "Chayen", 30);
        Product p2 = new Product(2, "Amerigano", 40);

        User seller = new User("Naded", "080460211", "password");
        Customer customer = new Customer("Naded", "080460211");
        Receipt receipt = new Receipt(seller, customer);

        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.print(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.print(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.print(receipt);
    }
}
